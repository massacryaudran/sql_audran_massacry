-- Ajout de données dans la table "entrepots"
-- données : nom, adresse, ville et pays

INSERT INTO entrepots (nom_entrepot, adresse, ville, pays) VALUES
    ('Entrepot 1', 'Adresse 1', 'Paris', 'France'),
    ('Entrepot 2', 'Adresse 2', 'Barcelone', 'Espagne'),
    ('Entrepot 3', 'Adresse 3', 'Moscou', 'Russie'),
    ('Entrepot 4', 'Adresse 4', 'Berlin', 'Allemagne'),
    ('Entrepot 5', 'Adresse 5', 'Lisbonne', 'Portugal'),

    -- rajout des pays asiatiques pour les requêtes complexes
    ('Entrepot 6', 'Adresse 6', 'Pekin', 'Chine'),
    ('Entrepot 7', 'Adresse 7', 'Tokyo', 'Japon'),
    ('Entrepot 8', 'Adresse 8', 'Séoul', 'Corée du Sud'),
    ('Entrepot 9', 'Adresse 9', 'New Delhi', 'Inde'),
    ('Entrepot 10', 'Adresse 10', 'Bangkok', 'Thaïland'),

    -- rajout des pays européens pour les requêtes complexes
    ('Entrepot 11', 'Adresse 11', 'Lyon', 'France'),
    ('Entrepot 12', 'Adresse 12', 'Valence', 'Espagne'),
    ('Entrepot 13', 'Adresse 13', 'Munich', 'Allemagne'),
    ('Entrepot 14', 'Adresse 14', 'Porto', 'Portugal');


-- Ajout de données dans la table "expeditions"
-- données : date expedition, id clé étrangère source, id clé étrangère destination, poids et statut

INSERT INTO expeditions (date_expedition, id_entrepot_source, id_entrepot_destination, poids, statut, date_fin_expedition, retard) VALUES
    ('2023-01-15', 3, 2, 500.5, 'Livré', '2023-01-17', 1),
    ('2023-02-15', 1, 5, 240.7, 'En cours', null, null),
    ('2023-03-15', 4, 2, 322.5, 'En attente', null, null),
    ('2023-04-15', 5, 3, 546.9, 'Livré', '2023-04-26', 4),
    ('2023-05-15', 5, 1, 110.0, 'Livré', '2023-05-18', null),
    ('2023-06-15', 1, 3, 560.5, 'En attente', null, null),
    ('2023-07-15', 4, 2, 292.0, 'En cours', null, null),
    ('2023-08-15', 1, 2, 211.0, 'En attente', null, null),
    ('2023-09-15', 5, 2, 455.0, 'Livré', '2023-09-16', null),
    ('2023-10-15', 3, 4, 197.0, 'En cours', null, null),

    -- Rajout des dates de livraison pour les requêtes complexes
    ('2023-01-15', 3, 6, 500.5, 'Livré', '2023-03-12', 3),
    ('2023-02-15', 1, 8, 640.7, 'En cours', null, null),
    ('2023-03-15', 4, 10, 322.5, 'En attente', null, null),
    ('2023-04-15', 5, 9, 546.9, 'Livré', '2023-06-12', 6),
    ('2023-05-15', 5, 7, 110.0, 'Livré', '2023-08-24', null),
    ('2023-06-15', 8, 3, 560.5, 'En attente', null, null),
    ('2023-07-15', 6, 2, 292.0, 'En cours', null, null),
    ('2023-08-15', 10, 2, 211.0, 'En attente', null, null),
    ('2023-09-15', 7, 2, 455.0, 'Livré', '2023-10-12', 2),
    ('2023-10-15', 9, 4, 197.0, 'En cours', null, null),
   
    -- Rajout des dates de livraison pour les requêtes complexes
    ('2023-01-15', 1, 11, 500.5, 'Livré', '2023-01-17', null),
    ('2023-01-15', 2, 12, 500.5, 'Livré', '2023-01-17', null),
    ('2023-01-15', 4, 13, 500.5, 'Livré', '2023-01-17', null),
    ('2023-01-15', 5, 14, 500.5, 'Livré', '2023-01-17', null),

    -- Rajout des dates de livraisons pour la requête complexe "30 derniers jours et + de 1000kg"
    -- Update ces dates lors de la notation si inférieur à 30 jours
    ('2023-10-20', 3, 2, 500.5, 'Livré', '2023-11-03', null),
    ('2023-10-21', 3, 2, 500.5, 'Livré', '2023-11-03', null),
    ('2023-10-22', 3, 2, 500.5, 'Livré', '2023-11-03', null);



-- Données pour les requêtes complexes (requete : Affichez le nombre total d'expéditions pour chaque jour du mois en cours, trié par ordre décroissant.)
-- Changer le mois pour correspondre au mois en cours

INSERT INTO expeditions (date_expedition, date_fin_expedition, id_entrepot_source, id_entrepot_destination, poids, statut) VALUES
    ('2023-11-01', '2023-11-03', 3, 2, 500.5, 'Livré'),
    ('2023-11-01', '2023-11-02', 1, 5, 240.7, 'Livré'),
    ('2023-11-02', '2023-11-05', 4, 2, 322.5, 'En cours'),
    ('2023-11-03', '2023-11-10', 5, 3, 546.9, 'Livré'),
    ('2023-11-05', '2023-11-08', 5, 1, 110.0, 'Livré'),
    ('2023-11-10', '2023-11-15', 1, 3, 560.5, 'En cours'),
    ('2023-11-12', NULL, 4, 2, 292.0, 'En cours'),
    ('2023-11-15', NULL, 1, 2, 211.0, 'En attente'),
    ('2023-11-20', '2023-11-25', 5, 2, 455.0, 'Livré'),
    ('2023-11-21', '2023-11-26', 3, 4, 197.0, 'Livré'),
    ('2023-11-22', NULL, 2, 5, 300.0, 'En cours'),
    ('2023-11-24', '2023-11-28', 3, 1, 400.0, 'Livré');

