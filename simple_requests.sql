
-- Affichez tous les entrepôts 
SELECT * FROM entrepots;

-- Affichez toutes les expéditions
SELECT * FROM expeditions;

-- Affichez toutes les expéditions avec le statut "En cours"
SELECT * FROM expeditions WHERE statut = 'En cours';

-- Affichez toutes les expéditions avec le statut "Livré"
SELECT * FROM expeditions WHERE statut = 'Livré';
