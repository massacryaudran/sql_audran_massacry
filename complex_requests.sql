-- Affiche les expéditions en transit initié en Europe et à destination de l'Asie.

SELECT exp.*
FROM expeditions AS exp
INNER JOIN entrepots AS ent_source ON exp.id_entrepot_source = ent_source.id
INNER JOIN entrepots AS ent_destination ON exp.id_entrepot_destination = ent_destination.id
WHERE ent_source.pays IN ('France', 'Espagne', 'Russie', 'Allemagne', 'Portugal')
    AND ent_destination.pays IN ('Chine', 'Japon', 'Inde', 'Corée du Sud', "Thaïlande")
    AND exp.statut = 'En cours';


-- Affiche les expeditions à destination du même pays que l'entrepôt source.

SELECT ent.*
FROM entrepots AS ent
INNER JOIN expeditions AS exp ON ent.id = exp.id_entrepot_source
INNER JOIN entrepots AS dest ON exp.id_entrepot_destination = dest.id
WHERE ent.pays = dest.pays;


-- Affiche les expeditions à destination d'un pays différent de celui de l'entrepôt source.

SELECT ent.*
FROM entrepots AS ent
INNER JOIN expeditions AS exp ON ent.id = exp.id_entrepot_source
INNER JOIN entrepots AS dest ON exp.id_entrepot_destination = dest.id
WHERE ent.pays != dest.pays
GROUP BY ent.id;


-- Affiche les expeditions initié par un pays commençant par F et dont le poids est supérieur à 500 kg et dont le statut est "En cours".

SELECT exp.*
FROM expeditions AS exp
INNER JOIN entrepots AS ent ON exp.id_entrepot_source = ent.id
WHERE ent.pays LIKE 'F%'
    AND exp.poids > 500
    AND exp.statut = 'En cours';


-- Affiche le total d'expeditions pour chaque combinaison de pays d'origine et de destination.

SELECT ent_src.pays AS pays_origine, ent_dest.pays AS pays_destination, COUNT(*) AS nombre_expeditions
FROM expeditions AS exp
INNER JOIN entrepots AS ent_src ON exp.id_entrepot_source = ent_src.id
INNER JOIN entrepots AS ent_dest ON exp.id_entrepot_destination = ent_dest.id
GROUP BY ent_src.pays, ent_dest.pays;


-- Affiche les entrepôts ayant envoyé plus de 1000 kg d'expéditions sur les 30 derniers jours.

SELECT ent.id, ent.nom_entrepot, SUM(ex.poids) AS poids_total
FROM entrepots AS ent
JOIN expeditions AS ex ON ent.id = ex.id_entrepot_source
WHERE ex.date_expedition >= DATE_SUB(NOW(), INTERVAL 30 DAY)
GROUP BY ent.id, ent.nom_entrepot
HAVING poids_total > 1000;


-- Affiche les expitions avec un statut "Livré" et plus de 2 jours de retard

SELECT *
FROM expeditions
WHERE statut = 'Livré' AND retard > 2;  


-- Affiche le nombre total d'expéditions pour chaque jour du mois en cours, trié par ordre décroissant

SELECT
    DAY(date_expedition) AS jour,
    COUNT(*) AS nombre_d_expeditions
FROM expeditions
WHERE YEAR(date_expedition) = YEAR(CURDATE()) AND MONTH(date_expedition) = MONTH(CURDATE())
GROUP BY DAY(date_expedition)
ORDER BY DAY(date_expedition) DESC;
