-- Ajout de la table "clients"

CREATE TABLE clients (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50),
    adresse VARCHAR(50),
    ville VARCHAR(75),
    pays VARCHAR(50)
);


-- Ajout table de jointure

CREATE TABLE expeditions_clients (
    id_expedition INT,
    id_client INT,
    FOREIGN KEY (id_expedition) REFERENCES expeditions(id),
    FOREIGN KEY (id_client) REFERENCES clients(id)
);


-- Modification de la table "expeditions" et ajout colonne foreign key

ALTER TABLE expeditions
ADD COLUMN id_client INT,
ADD FOREIGN KEY (id_client) REFERENCES clients(id);


-- Ajoute de données aux tables "clients" et "expeditions_clients"

INSERT INTO clients (nom, adresse, ville, pays) VALUES
    ('Client 1', 'Adresse 1', 'Paris', 'France'),
    ('Client 2', 'Adresse 2', 'Barcelone', 'Espagne'),
    ('Client3', 'Adresse 3', 'Moscou', 'Russie'),
    ('Client4', 'Adresse 4', 'Berlin', 'Allemagne'),
    ('Client5', 'Adresse 5', 'Lisbonne', 'Portugal'),
    ('Client6', 'Adresse 6', 'Pekin', 'Chine'),
    ('Client7', 'Adresse 7', 'Tokyo', 'Japon'),
    ('Client8', 'Adresse 8', 'Séoul', 'Corée du Sud'),
    ('Client9', 'Adresse 9', 'New Delhi', 'Inde'),
    ('Client10', 'Adresse 10', 'Bangkok', 'Thaïland'),
    ('Client11', 'Adresse 11', 'Lyon', 'France'),
    ('Client12', 'Adresse 12', 'Valence', 'Espagne'),
    ('Client13', 'Adresse 13', 'Munich', 'Allemagne'),
    ('Client14', 'Adresse 14', 'Porto', 'Portugal');


INSERT INTO expeditions_clients (id_expedition, id_client) VALUES
    (1, 2),
    (2, 5),
    (3, 2),
    (4, 3),
    (5, 1),
    (6, 3),
    (7, 2),
    (8, 2),
    (9, 2),
    (10, 4),
    (11, 6),
    (12, 8),
    (13, 10),
    (14, 9);


-- afficher pour chaque client sont nom, adresse complète (adresse, ville et pays), le total d'expéditions envoyées et le total d'expéditions reçues.

SELECT c.nom, CONCAT(c.adresse, ', ', c.ville, ', ', c.pays) AS adresse,
    COUNT(ec_envoyee.id_expedition) AS total_expeditions_envoyees,
    COUNT(ec_recue.id_expedition) AS total_expeditions_recues
FROM clients AS c
LEFT JOIN expeditions_clients AS ec_envoyee ON c.id = ec_envoyee.id_client
LEFT JOIN expeditions_clients AS ec_recue ON c.id = ec_recue.id_client
GROUP BY c.id;


-- Pour chaque expédition, affichez son ID, son poids, le nom du client qui l'a initiée et le nom du client destinataire.

-- Dans un premier temps, il faut mettre à jour les données de la table "expeditions" pour insérer les données clients dans la nouvelle colonne "id_client"

UPDATE expeditions SET id_client='3' WHERE id='1';
UPDATE expeditions SET id_client='1' WHERE id='2';
UPDATE expeditions SET id_client='4' WHERE id='3';
UPDATE expeditions SET id_client='5' WHERE id='4';
UPDATE expeditions SET id_client='5' WHERE id='5';
UPDATE expeditions SET id_client='1' WHERE id='6';
UPDATE expeditions SET id_client='4' WHERE id='7';
UPDATE expeditions SET id_client='1' WHERE id='8';
UPDATE expeditions SET id_client='5' WHERE id='9';
UPDATE expeditions SET id_client='3' WHERE id='10';
UPDATE expeditions SET id_client='3' WHERE id='11';
UPDATE expeditions SET id_client='1' WHERE id='12';
UPDATE expeditions SET id_client='4' WHERE id='13';
UPDATE expeditions SET id_client='5' WHERE id='14';

-- Ensuite, exécuter la requête pour aficher ce qui est demandé (seul les 14 premières expéditions possèdent les données des clients)

SELECT exp.id, exp.poids, exp.statut,
    c_sender.nom AS client_expediteur, 
    c_receiver.nom AS client_destinataire
FROM expeditions exp
LEFT JOIN clients c_sender ON exp.id_client = c_sender.id
LEFT JOIN expeditions_clients AS exp_cli ON exp.id = exp_cli.id_expedition
LEFT JOIN clients c_receiver ON exp_cli.id_client = c_receiver.id;


