-- Création de la bdd si elle n'existe pas

CREATE DATABASE IF NOT EXISTS transport_logistique;
USE transport_logistique;
 

-- Création de la table "entrepots" dans la bdd "transport_logistique"
-- id: int clé primaire / nom_entrepo : varchar / adresse : varchar / ville : varchar / pays : varchar

CREATE TABLE entrepots (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom_entrepot VARCHAR(100),
    adresse VARCHAR(100),
    ville VARCHAR(50),
    pays VARCHAR(50)
);


-- Création de la table "expeditions"
-- id: int clé primaire / date_expedition : date / id_entrepot_source : int clé étrangère / id_entrepot_destination : int clé étrangère / poids : decimal / statut : varchar

CREATE TABLE expeditions (
    id INT AUTO_INCREMENT PRIMARY KEY,
    date_expedition DATE,
    date_fin_expedition DATE,
    retard INT,
    id_entrepot_source INT,
    id_entrepot_destination INT,
    poids DECIMAL(10, 2),
    statut VARCHAR(255),
    FOREIGN KEY (id_entrepot_source) REFERENCES entrepots(id),
    FOREIGN KEY (id_entrepot_destination) REFERENCES entrepots(id)
);