-- Affiche les entrepôts ayant au moins une expedition avec le statut "En cours"

SELECT DISTINCT ent.id, ent.nom_entrepot
FROM entrepots AS ent
INNER JOIN expeditions AS exp ON ent.id = exp.id_entrepot_source
WHERE exp.statut = 'En cours';


-- Affiche les entrepôts ayant reçu au moins une expédition avec le statut "En cours"

SELECT DISTINCT ent.id, ent.nom_entrepot
FROM entrepots AS ent
INNER JOIN expeditions AS exp ON ent.id = exp.id_entrepot_destination
WHERE exp.statut = 'En cours';


-- Affichez les expéditions ayant un poids supérieur à 100 kg et avec l statut "En cours"

SELECT *
FROM expeditions
WHERE poids > 100 AND statut = 'En cours';



-- Affiche le nombre d'expéditions envoyées par chaque entrepôt.

SELECT ent.id, ent.nom_entrepot, COUNT(exp.id) AS expeditions_envoyees
FROM entrepots AS ent
LEFT JOIN expeditions AS exp ON ent.id = exp.id_entrepot_source
GROUP BY ent.id;


-- Affiche le total des expéditions en transit.

SELECT COUNT(*) AS total_expeditions_transit
FROM expeditions
WHERE statut = 'En cours';


-- Affiche le total des expéditions livrées.

SELECT COUNT(*) AS total_expeditions_livrees
FROM expeditions
WHERE statut = 'Livré';


--  Sur l'année en cours, affiche le total des expeditions pour chaque mois

SELECT MONTH(date_expedition) AS month, COUNT(*) AS total_expeditions
FROM expeditions
WHERE YEAR(date_expedition) = YEAR(CURDATE())
GROUP BY MONTH(date_expedition);


-- Sur les 30 derniers jours, affiche le nom des entrepots ayant envoyé des expéditions

SELECT DISTINCT ent.id, ent.nom_entrepot
FROM entrepots AS ent
INNER JOIN expeditions AS exp ON ent.id = exp.id_entrepot_source
WHERE exp.date_expedition >= CURDATE() - INTERVAL 30 DAY;


-- Sur les 30 derniers jours, affiche le nom des entrepots ayant reçu des expéditions

SELECT DISTINCT ent.id, ent.nom_entrepot
FROM entrepots AS ent
INNER JOIN expeditions AS exp ON ent.id = exp.id_entrepot_destination
WHERE exp.date_expedition >= CURDATE() - INTERVAL 30 DAY;


-- Affiche les expéditions livrées sous moins de 5 jours

SELECT *
FROM expeditions
WHERE statut = 'Livré' AND DATEDIFF(date_fin_expedition, date_expedition) <= 5;
