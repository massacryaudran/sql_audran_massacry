# SQL_Audran_MASSACRY


## Documentation

### Création base de données et tables*

La première requête vérifie si la bdd existe. Si ce n'est pas le cas, celle-ci est créée et la bdd est uilisée pour les requêtes suivantes. 

Les deux tables principales sont ensuites créées avec leurs colonnes propres et les liens (la table "expeditions" est liées par 2 foreign key reliées à la primary key de la table "entrepots")


### Insertion de données

Afin de faire les requêtes suivantes, nous inserons dans chaque table des données. 

Des commentaires sont placés dans les insert pour expliquer à quoi m'ont servi chaque groupe de données sur le travail demandé. 

La table expeditions contient plus de colonnes que demandé pour correspondre aux requêtes suivantes : rajout des colonnes date_fin_expedition et retard. 

Enfin, une dernière requête INSERT est disponible et permet de répondre à la requête "affichage du nombre total d'expedition pour chaque jour du mois en cours". Veuillez donc changer les dates si elles ne correspondent plus au mois en cours. 


### Simple requests

1. **Affichez tous les entrepôts**

    Cette requête affiche l'intégralité des données de la table "entrepots". Vous pouvez consulter les informations détaillées de tous les entrepôts enregistrés dans la base de données.

2. **Affichez toutes les expéditions**

    Cette requête affiche l'intégralité des données de la table "expeditions". Vous pouvez consulter les informations complètes de toutes les expéditions enregistrées.

3. **Affichez toutes les expéditions avec le statut "En cours"**

    Cette requête affiche toutes les expéditions qui ont le statut "En cours". Vous pouvez ainsi obtenir la liste des expéditions actuellement en transit.

4. **Affichez toutes les expéditions avec le statut "Livré"**

    Cette requête affiche toutes les expéditions qui ont été livrées, c'est-à-dire celles ayant le statut "Livré". Cela vous permet de suivre les expéditions réussies.


### Advanced requests

1. **Entrepôts ayant au moins une expédition en cours**

    Cette requête affiche les entrepôts qui ont au moins une expédition en cours à partir de la table "expeditions". Cela vous permet de voir quels entrepôts sont actuellement actifs dans l'envoi d'expéditions en cours.

2. **Entrepôts ayant reçu au moins une expédition en cours**

    Cette requête affiche les entrepôts qui ont reçu au moins une expédition en cours à partir de la table "expeditions". Cela vous permet de voir quels entrepôts sont actuellement actifs en tant que destinataires d'expéditions en cours.

3. **Expéditions avec un poids supérieur à 100 kg et en cours**

    Cette requête affiche les expéditions ayant un poids supérieur à 100 kg et actuellement en cours. Cela vous permet de suivre les expéditions de grande envergure en cours.

4. **Nombre d'expéditions envoyées par chaque entrepôt**

    Cette requête affiche le nombre d'expéditions envoyées par chaque entrepôt à partir de la table "expeditions". Cela vous donne un aperçu du volume d'expéditions géré par chaque entrepôt.

5. **Total des expéditions en transit**

    Cette requête affiche le nombre total d'expéditions en cours (en transit) à partir de la table "expeditions". Cela vous permet de suivre les expéditions en cours dans l'ensemble du système.

6. **Total des expéditions livrées**

    Cette requête affiche le nombre total d'expéditions ayant le statut "Livré" à partir de la table "expeditions". Cela vous permet de suivre le nombre d'expéditions réussies.

7. **Total des expéditions par mois (sur l'année en cours)**

    Cette requête affiche le total des expéditions pour chaque mois de l'année en cours. Cela vous donne un aperçu de l'évolution du volume d'expéditions tout au long de l'année.

8. **Entrepôts ayant envoyé des expéditions au cours des 30 derniers jours**

    Cette requête affiche les entrepôts qui ont envoyé des expéditions au cours des 30 derniers jours. Cela vous permet de suivre l'activité récente des entrepôts en tant qu'expéditeurs.

    Cette requête a été faite avant le rajout de la colonne "date_fin_expedition" et est donc basée sur la colonne "date_expedition"

    Les données à insérer dans les entrepôts ont été modifiées pour correspondre à la requête. Je vous invite à modifier les valeurs de la colonne "date_expedition" si l'ensemble des données sont inférieures aux 30 derniers jours. 

9. **Entrepôts ayant reçu des expéditions au cours des 30 derniers jours**

    Cette requête affiche les entrepôts qui ont reçu des expéditions au cours des 30 derniers jours. Cela vous permet de suivre l'activité récente des entrepôts en tant que destinataires.

    Cette requête a été faite avant le rajout de la colonne "date_fin_expedition" et est donc basée sur la colonne "date_expedition"

    Les données à insérer dans les entrepôts ont été modifiées pour correspondre à la requête. Je vous invite à modifier les valeurs de la colonne "date_expedition" si l'ensemble des données sont inférieures aux 30 derniers jours. 

10. **Expéditions livrées sous moins de 5 jours**

    Cette requête affiche les expéditions ayant le statut "Livré" et ayant été livrées en moins de 5 jours. Cela vous permet de repérer les expéditions rapides.

    Afin de pouvoir calculer ce temps, j'ai ajouter à la table "expeditions" la colonne "date_fin_expedition". Cette colonne est maintenant ajoutée dans la requête de création de la table.


### Complex requests


1. **Expéditions en transit initiées en Europe et à destination de l'Asie**

    Cette requête affiche les expéditions qui sont actuellement en transit et qui ont été initiées en Europe (les pays européens sont directement spécifiés dans la requête) et sont destinées à l'Asie (les pays asiatiques sont directement spécifiés dans la requête). Cela vous permet de suivre les expéditions internationales.

    Pour cette requête, j'ai ajouté de nouveaux entrepôts en Asie. Je compare l'appartenance d'un pays à un continent directement dans la requête.

2. **Expéditions à destination du même pays que l'entrepôt source**

    Cette requête affiche les expéditions où l'entrepôt source et l'entrepôt de destination sont situés dans le même pays. Vous pouvez ainsi visualiser les expéditions nationales.

    Pour cete requête j'ai ajouté des entrepôts dans des villes situés dans des pays déjà créés.

3. **Expéditions à destination d'un pays différent de celui de l'entrepôt source**

    Cette requête affiche les expéditions où l'entrepôt source et l'entrepôt de destination sont situés dans des pays différents. Cela vous permet de suivre les expéditions internationales.

4. **Expéditions initiées par un pays commençant par "F" avec un poids supérieur à 500 kg et en cours**

    Ici, les expéditions initiées par un pays dont le nom commence par "F" (Dans notre cas, la France) et ayant un poids supérieur à 500 kg, tout en étant en cours, sont affichées. Cela peut vous aider à suivre les expéditions importantes en cours spécifique à certains pays.

5. **Total d'expéditions pour chaque combinaison de pays d'origine et de destination**

    Cette requête affiche le nombre total d'expéditions pour chaque combinaison de pays d'origine et de destination. Elle vous permet d'analyser les échanges commerciaux entre deux pays.

6. **Entrepôts ayant envoyé plus de 1000 kg d'expéditions au cours des 30 derniers jours**

    Ici, les entrepôts qui ont envoyé plus de 1000 kg d'expéditions au cours des 30 derniers jours sont affichés. Cela vous donne un aperçu des entrepôts les plus actifs récemment.

7. **Expéditions avec un statut "Livré" et plus de 2 jours de retard**

    Cette requête affiche les expéditions ayant le statut "Livré" et ayant plus de 2 jours de retard. Cela vous permet de suivre les expéditions n'ayant pas respecté les délais.

    Afin de pouvoir comparer les retards et faire ma requête, j'ai ajouté la colonne "retard" à la table "expeditions". De la même manière que la colonne "date_fin_expedition", la colonne "retard" est ajoutée dans la requête de création de la table. 

8. **Nombre total d'expéditions pour chaque jour du mois en cours, trié par ordre décroissant**

    Enfin, cette requête affiche le nombre total d'expéditions pour chaque jour du mois en cours, trié par ordre décroissant. Cela vous donne un aperçu de l'évolution quotidienne du volume d'expéditions au cours du mois actuel.


### Bonus requests

1. **Ajout de la table "clients"**

    Cette requête crée la table "clients" pour stocker des informations sur les clients, y compris leur nom, adresse, ville et pays.

2. **Ajout de la table de jointure "expeditions_clients"**

    Cette requête crée la table de jointure "expeditions_clients" pour établir des liens entre les expéditions et les clients. Cela permet de suivre les expéditions associées à chaque client.

3. **Modification de la table "expeditions" et ajout d'une colonne de clé étrangère**

    Cette requête modifie la table "expeditions" en ajoutant une colonne de clé étrangère "id_client" pour lier chaque expédition à un client.

4. **Ajout de données aux tables "clients" et "expeditions_clients**

    Ces requêtes ajoutent des données de clients et de relations entre les expéditions et les clients dans les nouvelles tables. Cela permet d'établir des connexions entre les clients et les expéditions.

5. **Afficher pour chaque client son nom, adresse complète, total d'expéditions envoyées et total d'expéditions reçues**

    Cette requête affiche pour chaque client son nom, son adresse complète (adresse, ville et pays), le total d'expéditions qu'il a initiées et le total d'expéditions qu'il a reçues. Cela fournit une vue d'ensemble des activités des clients dans le système.

6. **Pour chaque expédition, afficher son ID, son poids, le nom du client expéditeur et le nom du client destinataire**

    Avant d'exécuter cette requête, il est nécessaire de mettre à jour les données de la table "expeditions" en ajoutant les données des clients dans la nouvelle colonne "id_client". Cette requête affiche ensuite les détails de chaque expédition, y compris son ID, son poids, le nom du client qui l'a initiée (expéditeur) et le nom du client destinataire. Veuillez noter que seules les 14 premières expéditions contiennent des données clients.
